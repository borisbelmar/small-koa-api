FROM node:12-alpine

COPY ["package.json", "yarn.lock", "/app/"]

WORKDIR "/app"

RUN "yarn"

COPY ["./src/", "./src"]

EXPOSE 3000

CMD ["yarn", "dev"]