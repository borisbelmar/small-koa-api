const supertest = require('supertest')
const app = require('../app')

const request = supertest(app.callback())

describe('Testing app', () => {
  it('Some test', async done => {
    const res = await request.get('/')
    expect(res.status).toBe(200)
    expect(res.text).toBe('Ok')
    done()
  })
})