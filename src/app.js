const Koa = require('koa')
const koaBody = require('koa-body')
const cors = require('@koa/cors')

const responseTime = require('./middleware/responseTime')
const logger = require('./middleware/logger')

// Init
const app = new Koa()

// Middlewares
app.use(logger)
app.use(responseTime)
app.use(koaBody({ jsonLimit: '1kb' }))
app.use(cors())

// Routes
app.use(require('./routes'))

module.exports = app
