const jwt = require('jsonwebtoken')

const PRIVATE_KEY = process.env.SECRET_KEY

const tokenVerify = bearerToken => {
  if (bearerToken.includes('Bearer')) {
    const token = bearerToken.split(' ')[1]
    return jwt.verify(token, PRIVATE_KEY)
  }
  throw Error('No bearer token')
}

const tokenSign = payload => {
  const tokenPayload = {
    sub: payload.id
  }
  return jwt.sign(tokenPayload, PRIVATE_KEY, { expiresIn: '1h' })
}

module.exports = {
  tokenVerify,
  tokenSign
}
