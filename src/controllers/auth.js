const bcrypt = require('bcryptjs')

const { tokenSign } = require('../utils/jwt')
const User = require('../models/User')

const SALT_WORK_FACTOR = 10

const login = async ctx => {
  const payload = ctx.request.body
  if (payload) {
    const user = await User.findOne({ email: payload.email })
    if (user) {
      const passwordMatch = await bcrypt.compare(payload.password, user.password)
      if (passwordMatch) {
        const token = tokenSign(user)
        ctx.body = {
          accessToken: token
        }
        return
      }
    }
  }
  ctx.throw(401, 'INVALID_CREDENTIALS')
}

const signup = async ctx => {
  const payload = ctx.request.body
  if (payload) {
    try {
      const newUser = new User(payload)
      const salt = await bcrypt.genSalt(SALT_WORK_FACTOR)
      newUser.password = await bcrypt.hash(payload.password, salt)
      await newUser.save()
      ctx.status = 201
      ctx.body = newUser
      return
    } catch (err) {
      if (err.code === 11000) {
        ctx.status = 409
        ctx.body = 'EXISTENT_EMAIL'
        return
      }
      ctx.status = 500
      ctx.body = 'INTERNAL_ERROR'
      return
    }
  }
  ctx.status = 400
  ctx.body = 'BAD_REQUEST'
}

module.exports = {
  login,
  signup
}
