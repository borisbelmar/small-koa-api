const Note = require('../models/Note')

const getAll = async ctx => {
  const { sub } = ctx.user
  try {
    ctx.body = await Note.find({ uid: sub })
  } catch (err) {
    console.log(err)
    ctx.status = 500
    ctx.body = 'SERVER_ERROR'
  }
}

const getById = async ctx => {
  const { id } = ctx.params
  const { sub } = ctx.user
  try {
    ctx.body = await Note.findOne({ _id: id, uid: sub })
  } catch (err) {
    console.log(err)
    ctx.status = 500
    ctx.body = 'SERVER_ERROR'
  }
}

const save = async ctx => {
  const { sub } = ctx.user
  const payload = ctx.request.body
  try {
    const newNote = new Note({ ...payload, uid: sub })
    await newNote.save()
    ctx.status = 201
    ctx.body = newNote
    return
  } catch (err) {
    console.log(err)
    ctx.status = 500
    ctx.body = 'SERVER_ERROR'
  }
}

module.exports = {
  getAll,
  getById,
  save
}
