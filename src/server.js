require('dotenv').config()
const mongoose = require('mongoose')

const app = require('./app')

const PORT = process.env.PORT || 3000

// Mongoose init
mongoose.connect(process.env.MONGO, { useNewUrlParser: true, useUnifiedTopology: true })

app.listen(PORT, () => {
  console.info(`Server listening on port ${PORT} 🚀`)
})
