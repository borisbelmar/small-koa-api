const Router = require('@koa/router')

const { login, signup } = require('../controllers/auth')

const router = new Router()

router
  .post('/login', login)
  .post('/signup', signup)

module.exports = router.routes()
