const Router = require('@koa/router')

const protectRoute = require('../middleware/protectRoute')

const router = new Router()

router.get('/', ctx => {
  ctx.body = 'Ok'
})

router.use('/notes', protectRoute)

router.use(require('./auth'))
router.use(require('./notes'))

module.exports = router.routes()
