const Router = require('@koa/router')
const { getAll, getById, save } = require('../controllers/notes')

const router = new Router()

router
  .get('/notes', getAll)
  .post('/notes', save)
  .get('/notes/:id', getById)

module.exports = router.routes()
