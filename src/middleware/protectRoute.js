const { tokenVerify } = require('../utils/jwt')

const protectRoute = async (ctx, next) => {
  const bearertoken = ctx.get('authorization')
  try {
    const decoded = tokenVerify(bearertoken)
    ctx.user = decoded
    await next()
  } catch (err) {
    ctx.throw(403, err)
  }
}

module.exports = protectRoute
